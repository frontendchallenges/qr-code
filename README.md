# Frontend Mentor - QR Code card component solution

This is a solution to the [QR Code card component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/qr-code-component-iux_sIO_H). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)
- [Acknowledgments](#acknowledgments)

## Overview

### The challenge

Users should be able to:

- View the optimal layout depending on their device's screen size
- See hover and focus states for interactive elements

This is a basic project, but it was done to:
* Practice fundamental concepts, css and html
* Work with Vite
* Deploy to Gitlab pages

### Links

- Live Site URL: [Live Solution](https://frontendchallenges.gitlab.io/qr-code/)

## My process

1. Create project `yarn vite create`
   Add dependencies `yarn add --dev sass`
2. Create tokens.scss with the constant values for colors, fonts, and sizes, and styles.scss for general styling
  The styles.scss must be included in main.js and tokens.scss in vite.config.ts
3. Set a backgroud color for body
4. Think of the design by splitting the layout in Components:
```
    Main:
      Card (white background, rounded border)
        Header
          img: rounded border
        Body
          title: color black
          description: color gray
```          
5. Create a card center vertically and horizontally
6. Fortunately mobile and desktop are the same.
7. Deploy to gitlab pages (remember configure the base, e.g. vite build --base=/qr-code/)

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first workflow
- Vue + Vite

### Lessons

To use the router, I installed vou-router, then used in the main.ts, create a router/index.js with the routes. To create the links, you can use router-link or programatically router.push
To build the rating buttons, I used a radio button group, with CSS, I hided the checkbox and showed only the labels (the numbers) and converted in a state button with the :checked pseudo.

```js
import { useRouter } from "vue-router";

const router = useRouter();
...
router.push({
      name: "thank-you",
      params: { rate: selectedRate.value },
    });
```

How to center vertically a div
```css
 .parent-div {
  display: flex;
  flex-direction: column;
  height: 100vh;
}
```

### Continued development

This is one of many projects, I plan to build to make more experience with FrontEnd development, specially with the Framework, handling spacing and practicing css.

**Note: Delete this note and the content within this section and replace with your own plans for continued development.**

### Useful resources

- [How to deploy a Static Site](https://vitejs.dev/guide/static-deploy.html)
- [Router](https://next.router.vuejs.org/guide/essentials/passing-props.html#named-views)

## Author

- Website - [Víctor HG](https://gitlab.com/vianhg)
- Frontend Mentor - [@vianhg](https://www.frontendmentor.io/profile/vianhg)

## Acknowledgments

Always to God and my family.